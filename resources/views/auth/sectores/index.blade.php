@extends('auth.layout.layout')

@section('content')
    <h2>Sectores</h2>
<div class="card">
    <div class="card-body">
        <p class="card-descrpition">
            <a class="btn btn-outline-success btn-sm btn-rounded" href="{{route('sector.create')}}"><i class="mdi mdi-plus"></i> Alta Sector</a><br>
        </p>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Empresas</th>
                <th>Acciones</th>
            </tr>
            </thead>
            @forelse($sectores as $sector)
                <tbody>
                <tr>
                    <td><a class="btn btn-outline-info btn-sm btn-rounded" href="">{{$sector->id}}</a></td>
                    <td>{{$sector->nombre}}</td>
                    <td>{{$sector->empresas->count()}}</td>
                    <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('sector.edit', $sector)}}"><i class="mdi mdi-border-color"></i> Editar</a> <br>
                        {!! Form::open(['route' => ['sector.destroy', $sector], 'method' => 'DELETE']) !!}
                        <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i> Dar de baja</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
                </tbody>
            @empty
            @endforelse
        </table>
    </div>
</div>
@endsection
