@include('partials.errors')
@if(isset($sector))
    {{ Form::model($sector, ['route' => ['sector.update', $sector->id], 'method' => 'PUT']) }}
@else
    {!! Form::open(['route' => 'sector.store', 'method' => 'POST']) !!}
@endif

@csrf
    {!! Form::label('nombre', 'Nombre',['class'=>'']) !!}
    {!! Form::text('nombre', old('nombre'), ['class' => "",'required']) !!}
    <br><br>
{!! Form::submit(isset($empresa) ? 'Actualizar datos' : 'Dar de alta!'); !!}
{!! Form::close() !!}
