<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Purple Admin</title>
<!-- plugins:css -->
<link rel="stylesheet" href="{{url("admin/css/materialdesignicons.min.css")}}">
<link rel="stylesheet" href="{{url("admin/css/vendor.bundle.base.css")}}">
<link rel="stylesheet" href="{{url("admin/css/style.css")}}">
<link rel="shortcut icon" sizes="114x114" href="{{url("admin/img/logo.svg")}}">
