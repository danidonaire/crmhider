@include('partials.errors')

@if(isset($publicacion))
    {{ Form::model($publicacion, ['route' => ['publicaciones.update', $publicacion->id], 'method' => 'PUT']) }}
@else
    {!! Form::open(['route' => 'publicaciones.store', 'method' => 'POST']) !!}
@endif

@csrf


<fieldset>
<legend>Datos Personales:</legend>
    {!! Form::label('id', 'id',['class'=>'']) !!}
    {!! Form::text('id', isset($publicacion) ? $publicacion->id: old('id'), ['class' => "",'required', 'readonly'=>'true']) !!}
    <br><br>
    {!! Form::label('titulo', 'Titulo',['class'=>'']) !!}
    {!! Form::text('titulo', old('titulo'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('contenido', 'Contenido',['class'=>'']) !!}
    {!! Form::text('contenido', old('contenido'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('filtro', 'Filtro',['class'=>'']) !!}
    {!! Form::text('filtro', old('filtro'), ['class' => "",'']) !!}

</fieldset>

<fieldset>
    <legend>Categoria (falta select):</legend>

    <br><br>
</fieldset>




{!! Form::submit(isset($publicacion) ? 'Actualizar datos' : 'Dar de alta!'); !!}
{!! Form::close() !!}
