@extends('auth.layout.layout')

@section('content')
    <h2>Publicaciones </h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href="{{route('publicaciones.create')}}"><i class="mdi mdi-plus"></i> Alta Publicacion</a><br>
            </p>
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Titulo</th>
                    <th>User</th>
                    <th>Reputacion</th>
                    <th>Views</th>
                    <th>Filtro</th>
                    <th>Viral</th>
                    <th>Guardados</th>
                    <th>Votos</th>
                    <th>Acciones</th>
                </tr>
                @forelse($publicaciones as $publicacion)
                    <tr>
                        <td><a href="{{route('publicaciones.show', $publicacion)}}">{{$publicacion->id}}</a></td>
                        <td>{{$publicacion->titulo}}</td>
                        <td><a href="{{route('users.show', $publicacion->user->id)}}">{{$publicacion->user->nombre}}</a></td>
                        <td>{{$publicacion->reputacion}}</td>
                        <td>{{$publicacion->views}}</td>
                        <td>{{$publicacion->filtro}}</td>
                        <td>{{$publicacion->viral}}</td>
                        <td>{{$publicacion->guardados}}</td>
                        <td>{{count($publicacion->votos()->get())}}</td>
                        <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('publicaciones.edit', $publicacion)}}"><i class="mdi mdi-border-color"></i>Editar</a> <br>

                            {!! Form::open(['route' => ['publicaciones.destroy', $publicacion->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i> Dar de baja</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @empty
                @endforelse
            </table>
        </div>
    </div>
@endsection

