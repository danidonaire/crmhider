@extends('auth.layout.layout')

@section('content')
    <h2>Estadisticas</h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href="#"><i class="mdi mdi-plus"></i> Nuevo Chart</a><br>
            </p>
            <h1 class="h1">Aqui van los charts</h1>
        </div>
    </div>
@endsection
