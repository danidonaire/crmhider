@extends('auth.layout.layout')

@section('content')
    <h2>Empresas</h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href="{{route('empresa.create')}}"><i class="mdi mdi-plus"></i> Alta Empresa</a><br>
            </p>
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Token</th>
                    <th>Jefes</th>
                    <th>Direccion</th>
                    <th>Sectores</th>
                    <th>Acciones</th>
                </tr>
                @forelse($empresas as $empresa)
                    <tr>
                        <td><a href="{{route('empresa.show', $empresa)}}">{{$empresa->id}}</a></td>
                        <td>{{$empresa->nombre}}</td>
                        <td>{{$empresa->token}}</td>
                        <td>{{$empresa->jefes->pluck('fullname')->implode(' ,')}}</td>
                        <td>{{$empresa->direccion->FullStreet}}</td>
                        <td>{{$empresa->sectores->pluck('nombre')->implode(' ,')}}</td>
                        <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('empresa.edit', $empresa)}}"><i class="mdi mdi-border-color"></i>Editar</a> <br>

                            {!! Form::open(['route' => ['empresa.destroy', $empresa], 'method' => 'DELETE']) !!}
                                <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i>Dar de baja</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @empty
                @endforelse
            </table>
        </div>
    </div>
@endsection
