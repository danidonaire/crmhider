@include('partials.errors')
@if(isset($empresa))
    {{ Form::model($empresa, ['route' => ['empresa.update', $empresa->id], 'method' => 'PUT']) }}
@else
    {!! Form::open(['route' => 'empresa.store', 'method' => 'POST']) !!}
@endif

@csrf

<fieldset>
    <legend>Datos Empresa:</legend>
    {!! Form::label('nombre', 'Nombre',['class'=>'']) !!}
    {!! Form::text('nombre', old('nombre'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('token', 'Token',['class'=>'']) !!}
    {!! Form::text('token', old('token'), ['class' => "",'required']) !!}
</fieldset>

<fieldset>
    <legend>Dirección de la Empresa:</legend>
    @if(isset($empresa))
        {!! Form::hidden('direccion_id', 'direccion_id',['class'=>'']) !!}
        {!! Form::hidden('direccion_id', isset($empresa) ? $empresa->direccion->id : old('direccion_id'), ['class' => "",'required', 'readonly'=>'true']) !!}
        <br><br>
    @endif
    {!! Form::label('calle', 'Calle',['class'=>'']) !!}
    {!! Form::text('calle', isset($empresa) ? $empresa->direccion->calle : old('calle'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('numero', 'Numero',['class'=>'']) !!}
    {!! Form::text('numero', isset($empresa) ? $empresa->direccion->numero : old('numero'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('municipio', 'Municipio',['class'=>'']) !!}
    {!! Form::text('municipio', isset($empresa) ? $empresa->direccion->municipio : old('municipio'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('poblacion', 'Poblacion',['class'=>'']) !!}
    {!! Form::text('poblacion', isset($empresa) ? $empresa->direccion->poblacion : old('poblacion'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('cp', 'Código postal',['class'=>'']) !!}
    {!! Form::text('cp', isset($empresa) ? $empresa->direccion->cp : old('cp'), ['class' => "",'required']) !!}
    <br><br>
    {!! Form::label('pais', 'País',['class'=>'']) !!}
    {!! Form::text('pais', isset($empresa) ? $empresa->direccion->pais : old('pais'), ['class' => "",'required']) !!}
    <br><br>
</fieldset>

<fieldset>
    <legend>Sector:</legend>
    <select name="sector_id[]" id="sector_id[]" class="" multiple required>
        @foreach($sectores as $id => $sector)
            <option value="{{$id}}"
                {{old('sector_id') == $id || (isset($empresa) && $empresa->sectores->pluck('id')->contains($id)) ? "selected" : ''}}
            >{{$sector}}</option>
        @endforeach
    </select>
</fieldset>
{!! Form::submit(isset($empresa) ? 'Actualizar datos' : 'Dar de alta!'); !!}
{!! Form::close() !!}
