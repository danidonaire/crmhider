@extends('auth.layout.layout')

@section('content')
    <h2>Roles</h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href="{{route('roles.create')}}"><i class="mdi mdi-plus"></i> Alta Rol</a><br>
            </p>
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Nombre a mostrar</th>
                    <th>Permisos</th>
                    <th>Acciones</th>
                </tr>
                @forelse($roles as $role)
                    <tr>
                        <td>{{$role->id}}</td>
                        <td>{{$role->name}}</td>
                        <td>{{$role->description}}</td>
                        <td>{{$role->display_name}}</td>
                        <td>{{$role->permisos->pluck('display_name')->implode(' ,')}}</td>
                        <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('roles.edit', $role)}}"><i class="mdi mdi-border-color"></i>Editar</a> <br>

                            {!! Form::open(['route' => ['roles.destroy', $role], 'method' => 'DELETE']) !!}
                                <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i>Dar de baja</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @empty
                @endforelse
            </table>
        </div>
    </div>
@endsection
