@extends('auth.layout.layout')

@section('content')
    <h2>{{$user->nickname}}</h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href="{{route('users.create')}}"><i class="mdi mdi-plus"></i> Alta User</a><br>
            </p>
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Reputacion</th>
                    <th>Direccion</th>
                    <th>Fecha Nacimiento</th>
                    <th>Acciones</th>
                </tr>

                    <tr>
                        <td>{{$user->id}}</a></td>
                        <td>{{$user->nombre}}</td>
                        <td>{{$user->apellidos}}</td>
                        <td>{{$user->edad}}</td>
                        <td>{{$user->direccion->municipio}}, {{$user->direccion->cp}}</td>
                        <td>{{$user->fecha_nacimiento}}</td>
                        <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('users.edit', $user)}}"><i class="mdi mdi-border-color"></i>Editar</a> <br>
                            {!! Form::open(['route' => ['users.destroy', $user], 'method' => 'DELETE']) !!}
                            <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i>Dar de baja</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
            </table>
        </div>
    </div>
@endsection
