@include('partials.errors')
@if(isset($cliente))
    {{ Form::model($cliente, ['route' => ['cliente.update', $cliente->id], 'method' => 'PUT']) }}
@else
    {!! Form::open(['route' => 'cliente.store', 'method' => 'POST']) !!}
@endif

@csrf

    <fieldset>
        <legend>Datos Personales:</legend>
        {!! Form::label('nombre', 'Nombre',['class'=>'']) !!}
        {!! Form::text('nombre', old('nombre'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('apellidos', 'Apellidos',['class'=>'']) !!}
        {!! Form::text('apellidos', old('apellidos'), ['class' => "",'']) !!}

    </fieldset>

    <fieldset>
        <legend>Datos Sociales:</legend>
        {!! Form::label('email', 'Email',['class'=>'']) !!}
        {!! Form::email('email', old('email'), ['class' => "",'']) !!}
        <br><br>
        {!! Form::label('instagram', 'Instagram',['class'=>'']) !!}
        {!! Form::text('instagram', old('instagram'), ['class' => "",'']) !!}

    </fieldset>


        {!! Form::submit(isset($cliente) ? 'Actualizar datos' : 'Dar de alta!'); !!}
{!! Form::close() !!}
