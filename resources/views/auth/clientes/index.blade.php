@extends('auth.layout.layout')

@section('content')
    <h2>Clientes</h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href=""><i class="mdi mdi-plus"></i> Alta Cliente</a><br>
            </p>
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Email</th>
                    <th>Ingstagram</th>
                    <th>Fecha Nacimiento</th>
                    <th>Acciones</th>
                </tr>
                @forelse($clientes as $cliente)

                    <tr>
                        <td><a href="{{route('cliente.show', $cliente)}}">{{$cliente->id}}</a></td>
                        <td>{{$cliente->nombre}}</td>
                        <td>{{$cliente->apellidos}}</td>
                        <td>{{$cliente->email}}</td>
                        <td>{{$cliente->instagram}}</td>
                        <td>{{$cliente->fecha_nacimiento}}</td>
                        <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('cliente.edit', $cliente)}}"><i class="mdi mdi-border-color"></i>Editar</a> <br>
                            {!! Form::open(['route' => ['cliente.destroy', $cliente], 'method' => 'DELETE']) !!}
                                <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i>Dar de baja</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @empty
                @endforelse
            </table>
        </div>
    </div>
@endsection
