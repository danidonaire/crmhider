@include('partials.errors')
@if(isset($user))
    {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) }}
@else
    {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
@endif

@csrf

    <legend>Dirección del Usuario:</legend>
    <!--@if(isset($user))
        {!! Form::label('direccion_id', 'direccion_id',['class'=>'']) !!}
        {!! Form::text('direccion_id', isset($empresa) ? $empresa->direccion->id : old('direccion_id'), ['class' => "",'required', 'readonly'=>'true']) !!}
        <br><br>
    @endif-->

    <fieldset>
        <legend>Datos Personales:</legend>
        {!! Form::label('nombre', 'Nombre',['class'=>'']) !!}
        {!! Form::text('nombre', old('nombre'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('apellidos', 'Apellidos',['class'=>'']) !!}
        {!! Form::text('apellidos', old('apellidos'), ['class' => "",'required']) !!}

    </fieldset>

    <fieldset>
        <legend>Datos Sociales:</legend>
        {!! Form::label('email', 'Email',['class'=>'']) !!}
        {!! Form::email('email', old('email'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('nickname', 'Nickname',['class'=>'']) !!}
        {!! Form::text('nickname', old('nickname'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('password', 'Password',['class'=>'']) !!}
        {!! Form::text('password', old(''), ['class' => "",'required']) !!}
        <br><br>
    </fieldset>


    <fieldset>
        <legend>Dirección del Usuario:</legend>
        @if(isset($user))
            {!! Form::hidden('direccion_id', 'direccion_id',['class'=>'']) !!}
            {!! Form::hidden('direccion_id', isset($user) ? $user->direccion->id : old('direccion_id'), ['class' => "",'required', 'readonly'=>'true']) !!}
            <br><br>
        @endif
        {!! Form::label('calle', 'Calle',['class'=>'']) !!}
        {!! Form::text('calle', isset($user) ? $user->direccion->calle : old('calle'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('numero', 'Numero',['class'=>'']) !!}
        {!! Form::text('numero', isset($user) ? $user->direccion->numero : old('numero'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('municipio', 'Municipio',['class'=>'']) !!}
        {!! Form::text('municipio', isset($user) ? $user->direccion->municipio : old('municipio'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('poblacion', 'Poblacion',['class'=>'']) !!}
        {!! Form::text('poblacion', isset($user) ? $user->direccion->poblacion : old('poblacion'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('cp', 'Código postal',['class'=>'']) !!}
        {!! Form::text('cp', isset($user) ? $user->direccion->cp : old('cp'), ['class' => "",'required']) !!}
        <br><br>
        {!! Form::label('pais', 'País',['class'=>'']) !!}
        {!! Form::text('pais', isset($user) ? $user->direccion->pais : old('pais'), ['class' => "",'required']) !!}
        <br><br>
    </fieldset>


        {!! Form::submit(isset($user) ? 'Actualizar datos' : 'Dar de alta!'); !!}
{!! Form::close() !!}
