@extends('auth.layout.layout')

@section('content')
    <h2>Permisos</h2>
    <div class="card">
        <div class="card-body">
            <p class="card-descrpition">
                <a class="btn btn-outline-success btn-sm btn-rounded" href="{{route('permisos.create')}}"><i class="mdi mdi-plus"></i> Alta Permiso</a><br>
            </p>
            <table class="table table-hover">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Nombre a mostrar</th>
                    <th>Acciones</th>
                </tr>
                @forelse($permisos as $permiso)
                    <tr>
                        <td>{{$permiso->id}}</td>
                        <td>{{$permiso->name}}</td>
                        <td>{{$permiso->description}}</td>
                        <td>{{$permiso->display_name}}</td>
                        <td><a class="btn btn-outline-warning btn-sm btn-rounded" href="{{route('permisos.edit', $permiso)}}"><i class="mdi mdi-border-color"></i>Editar</a> <br>

                            {!! Form::open(['route' => ['permisos.destroy', $permiso], 'method' => 'DELETE']) !!}
                                <button class="btn btn-outline-danger btn-sm btn-rounded" type="submit"><i class="mdi mdi-close"></i>Dar de baja</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @empty
                @endforelse
            </table>
        </div>
    </div>
@endsection
