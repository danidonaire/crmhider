@include('partials.errors')
@if(isset($permission))
    {{ Form::model($permission, ['route' => ['permisos.update', $permission->id], 'method' => 'PUT']) }}
@else
    {!! Form::open(['route' => 'permisos.store', 'method' => 'POST']) !!}
@endif

@csrf

{!! Form::label('name', 'Nombre',['class'=>'']) !!}
{!! Form::text('name', old('name'), ['class' => "",'required']) !!}
<br><br>
{!! Form::label('display_name', 'Nombre a mostrar',['class'=>'']) !!}
{!! Form::text('display_name', old('display_name'), ['class' => "",'required']) !!}
<br><br>
{!! Form::label('description', 'Descripción',['class'=>'']) !!}
{!! Form::text('description', old('description'), ['class' => "",'required']) !!}

{!! Form::submit(isset($permission) ? 'Actualizar datos' : 'Dar de alta!'); !!}
{!! Form::close() !!}
