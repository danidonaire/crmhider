<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        //$this->call(PublicacionSeeder::class);
        //$this->call(VotoSeeder::class);
        $this->call(RoleSeeder::class);

        $this->call(PublicacionSeeder::class);
        $this->call(TipoVotoSeeder::class);

        //$this->call(VotoSeeder::class);
    }
}
