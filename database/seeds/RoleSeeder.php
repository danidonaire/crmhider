<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'administradores';
        $role->display_name = 'Rol para los administradores';
        $role->description = 'Rol para los administradores';
    }
}
