<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectores = [
            array('Agencias de publicidad‎'),
            array('Bancos‎‎'),
            array('Fabricantes de herramientas‎‎'),
        ];

        //Direccion ID 1
        $direccion = new \App\Direccion();
        $direccion->calle = 'Roger de Flor';
        $direccion->numero = '100-104';
        $direccion->municipio = 'Roquetes';
        $direccion->poblacion = 'Barcelona';
        $direccion->cp = '08800';
        $direccion->pais = 'España';
        $direccion->save();

        //Usuario id 1
        $user = new User();
        $user->nickname = 'User';
        $user->nombre = 'iiiiionel';
        $user->apellidos = 'Roberta';
        $user->fecha_nacimiento = now();
        $user->email = 'user@example.com';
        $user->password = bcrypt('123456');

        //Añadimos direccion a usuario
        $user->direccion_id= 1;

        $user->save();



        $empresa = new \App\Empresa();
        $empresa->nombre = 'Hider';
        $empresa->token = '123456789';
        $empresa->direccion_id = $direccion->id;
        $empresa->save();
        $empresa->jefes()->sync($user->id);

        foreach ($sectores as $sect){
            $sector = new \App\Sector();
            $sector->nombre = $sect[0];
            $sector->save();
        }


        $user = new User();
        $user->nickname = 'ionel';
        $user->nombre = 'ionel';
        $user->apellidos = 'duca';
        $user->fecha_nacimiento = now();
        $user->email = 'ducaionelrobert@gmail.com';
        $user->password = bcrypt('123456');
        $user->direccion_id= 1;
        $user->save();
    }
}
