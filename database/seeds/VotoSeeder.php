<?php

use Illuminate\Database\Seeder;
use App\Voto;

class VotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Publicacion id 1
        $voto= new Voto();

        $voto->user_id = 1;
        $voto->publicacion_id = 1;

        $voto->save();

    }
}
