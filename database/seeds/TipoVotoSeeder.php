<?php

use Illuminate\Database\Seeder;
use App\TipoVoto;

class TipoVotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo = new TipoVoto();
        $tipo->tipo= "Range";
        $tipo->save();

        $tipo = new TipoVoto();
        $tipo->tipo= "Viraly";
        $tipo->save();

    }
}
