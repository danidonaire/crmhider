<?php

use Illuminate\Database\Seeder;
use App\Publicacion;

class PublicacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Publicacion id 1
        $publicacion = new Publicacion();
        $publicacion->titulo = 'titulo';
        $publicacion->contenido = 'contenido';
        $publicacion->reputacion = 100;
        $publicacion->viral = 30;
        $publicacion->guardados = 3;
        $publicacion->filtro = 'new york';
        $publicacion->user_id = 1;

        $publicacion->save();

    }
}
