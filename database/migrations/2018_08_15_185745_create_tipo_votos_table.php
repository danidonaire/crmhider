<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoVotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_votos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->bigInteger('reputacion')->nullable();


            //peta -> $table->foreign('id_voto')->references('id_voto')->on('votos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_votos');
    }
}
