<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable();
            $table->string('contenido');
            $table->double('reputacion')->default(0); //como la medimos? (en hexadecimal?)
            $table->bigInteger('views')->default(0);
            $table->string('filtro')->nullanble()->default("ninguno");
            $table->double('viral')->default(0);
            $table->double('guardados')->default(0);  //Mi coleccion

            $table->string('categoria')->nullable();

            $table->unsignedInteger('user_id')->default(1);
            $table->foreign('user_id')->references('id')->on('users');



            //$table->double('categoria_publicacion');
            //$table->double('Votos');

            //$table->unsignedInteger('tipo_publicacion_id');

            //diferentes puntuaciones de una misma publicacion

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicaciones');
    }
}
