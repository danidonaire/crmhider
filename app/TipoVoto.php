<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoVoto extends Model
{
    protected  $table = 'tipo_votos';

    protected $primaryKey = 'id';

    protected $fillable = [
        'tipo', 'reputacion',
    ];

    public function votos(){
        return $this->hasMany(Votos::class);
    }

}
