<?php

namespace App\Repositories\Empresa;


interface EmpresaInterface
{
    public function getAll();
    public function store($request);
    public function destroy($id);
    public function findById($id);
    public function update($request, $empresa);
}
