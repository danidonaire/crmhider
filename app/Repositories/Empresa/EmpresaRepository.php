<?php
namespace App\Repositories\Empresa;


use App\Direccion;
use App\Empresa;

class EmpresaRepository implements EmpresaInterface
{

    public function getAll()
    {
        return Empresa::with(['direccion','jefes','sectores'])->get();
    }

    public function store($request)
    {
        $direccion = $this->createDirection($request);
        $empresa = new Empresa();
        if($direccion)
        {
            $empresa = new Empresa();
            $empresa->fill($request->all());
            $empresa->direccion_id = $direccion->id;
            $empresa->sectores()->sync($request->sector_id);
        }
        return $empresa->save();
    }

    public function findById($id)
    {
        return Empresa::with(['direccion','jefes','sectores'])->findOrFail($id);
    }

    public function update($request, $empresa)
    {
        $direccion = Direccion::findOrFail($request->direccion_id);
        $direccion->fill($request->all());
        $direccion->save();

        $empresa->fill($request->all());
        $empresa->direccion()->associate($direccion->id);
        $empresa->sectores()->sync($request->sector_id);

        return $empresa->save();
    }

    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    protected function createDirection($request)
    {
        $direccion = new Direccion();
        $direccion->fill($request->all());

        if($direccion->save()) return $direccion;

        return false;
    }
}
