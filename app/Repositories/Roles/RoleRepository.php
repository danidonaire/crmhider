<?php
namespace App\Repositories\Roles;


use App\Role;

class RoleRepository implements RoleInterface
{

    public function getAll()
    {
        return Role::with(['users','permisos'])->get();
    }

    public function store($request)
    {
        $role = new Role();
        $role->fill($request->all());
        $role->save();
        return $role->save();
    }

    public function update($request, $role)
    {
        $role->fill($request->all());
        return $role->save();
    }

    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    public function findById($id)
    {
        return Role::with(['users','permisos'])->findOrFail($id);
    }
}
