<?php

namespace App\Repositories\Roles;


interface RoleInterface
{
    public function getAll();
    public function store($request);
    public function update($request, $role);
    public function destroy($id);
    public function findById($id);
}
