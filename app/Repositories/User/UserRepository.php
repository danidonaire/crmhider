<?php
namespace App\Repositories\User;


use \App\Direccion;
use \App\User;


class UserRepository implements UserInterface
{

    public function getAll()
    {
        return User::with(['direccion'])->get();
    }

    public function store($request)
    {
        $direccion = $this->createDirection($request);
        $user = new User();
        if($direccion)
        {
            $user->fill($request->all());
            $user->direccion_id = $direccion->id;

        }
        return $user->save();
    }

    public function update($request, $user)
    {
        $direccion = Direccion::findOrFail($request->direccion_id);
        $direccion->fill($request->all());
        $direccion->save();

        $user->fill($request->all());
        $user->direccion()->associate($direccion->id);

        return $user->save();
    }

    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    public function findById($id)
    {
        return User::with(['direccion'])->findOrFail($id);
        }

    protected function createDirection($request)
    {
        $direccion = new Direccion();
        $direccion->fill($request->all());

        if($direccion->save()) return $direccion;

        return false;
    }


}
