<?php

namespace App\Repositories\User;


interface UserInterface
{
    public function getAll();
    public function store($request);
    public function update($request, $user);
    public function destroy($id);
    public function findById($id);
    //public function getDisplayNameAndId();
}
