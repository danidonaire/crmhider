<?php

namespace App\Repositories\Sector;


use App\Sector;

class SectorRepository implements SectorInterface
{

    public function getAll()
    {
        return Sector::with(['empresas'])->get();
    }

    public function getDisplayNameAndId()
    {
        return Sector::with(['empresas'])->pluck('nombre','id');
    }

    public function create($request)
    {
        $sector = new Sector();
        $sector->fill($request->all());
        return $sector->save();
    }

    public function update($request, $sector)
    {
        $sector->fill($request->all());
        return $sector->save();
    }

    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    public function findById($id)
    {
        return Sector::with(['empresas'])->findOrFail($id);
    }
}
