<?php
/**
 * Created by PhpStorm.
 * User: Ionel
 * Date: 20/09/2018
 * Time: 13:53
 */

namespace App\Repositories\Sector;


interface SectorInterface
{
    public function getAll();
    public function getDisplayNameAndId();
    public function create($request);
    public function update($request, $sector);
    public function destroy($id);
    public function findById($id);
}
