<?php

namespace App\Repositories\Permisos;


interface PermisosInterface
{
    public function getAll();
    public function store($request);
    public function update($request, $permiso);
    public function destroy($id);
    public function findById($id);
}
