<?php

namespace App\Repositories\Permisos;


use App\Permission;

class PermisosRepository implements PermisosInterface
{

    public function getAll()
    {
        return Permission::with('roles')->get();
    }

    public function store($request)
    {
        $permiso = new Permission();
        $permiso->fill($request->all());
        return $permiso->save();
    }

    public function update($request, $permiso)
    {
        $permiso->fill($request->all());
        return $permiso->save();
    }

    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    public function findById($id)
    {
        return Permission::with('roles')->findOrFail($id);
    }
}
