<?php

namespace App\Repositories\Publicacion;


interface PublicacionInterface
{
    public function getAll();
    public function store($request);
    public function destroy($id);
    public function findById($id);
    public function update($request, $publicacion);
}
