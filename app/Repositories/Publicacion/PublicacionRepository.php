<?php
namespace App\Repositories\Publicacion;


use \App\Publicacion;
use \App\User;
use \App\Votos;


class PublicacionRepository implements PublicacionInterface
{

    public function getAll()
    {
        return Publicacion::with(['user'])->get();
    }

    public function store($request)
    {
        $publicacion = new Publicacion();
        $publicacion->fill($request->all());

        return $publicacion->save();
    }

    public function update($request, $publicacion)
    {
        $publicacion->fill($request->all());
        return $publicacion->save();
    }

    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    public function findById($id)
    {
        return Publicacion::with(['user'])->findOrFail($id);
    }

    /*protected function createDirection($request)
    {
        $direccion = new Direccion();
        $direccion->fill($request->all());

        if($direccion->save()) return $direccion;

        return false;
    }
    */

}
