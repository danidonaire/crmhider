<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole{

    protected $table = 'roles';
    protected $fillable = [
        'name', 'display_name', 'description'
    ];

    public function permisos()
    {
        return $this->belongsToMany(Permission::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
