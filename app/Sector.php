<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'sectores';
    protected $fillable = [
        'nombre'
    ];

    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }

}
