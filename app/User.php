<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected  $table = 'users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre','nickname', 'apellidos', 'email' ,'reputacion','telefono','fecha_nacimiento','password'
    ];

    protected $hidden = [
        'remember_token',
    ];



    public function direccion()
    {
        return $this->belongsTo(Direccion::class);
    }
    public function roles(){
        return $this->belongsToMany(Role::class);
    }
    public function empresas(){
        return $this->belongsToMany(Empresa::class);
    }

    public function votos(){
        return $this->hasMany(Voto::class, 'id');
    }
    public function publicacion(){
        return $this->hasMany(Publicacion::class, 'id');
    }

    public function getFullNameAttribute(){
        return $this->attributes['nombre'].' '.$this->attributes['apellidos'];
    }

    public function setPasswordAttribute($password){
        $this->attributes['password'] = Hash::make($password);
    }

    public function username()
    {
        return 'nickname';
    }

}
