<?php

namespace App\Http\Controllers;

use App\User;
use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{

    //Necesario
    protected $userRepository;
    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->middleware('auth');
    }

    //100% funcional
    public function index()
    {
        $users = $this->userRepository->getAll();
        return view('auth.users.index',compact('users'));
    }

    //Lo tengo con dd porque Ionel el de show de empresa tambien, y sino me petaba por el foreach de la vista
    public function show(User $user)
    {
        $usuario = $this->userRepository->findById($user->id);
        //dd($usuario);
        return view('auth.users.index',compact('usuario'));
    }


    //Redirige bien hacia el formulario sin tener que pasar por el repositorio
    public function create()
    {
        return view('auth.users.form');
    }


    //100% funcional, da de alta pasando por repositorio
    //ALERTA - La contraseña de esta manera no se encripta
    public function store(Request $request)
    {
        if($this->userRepository->store($request)) return redirect()->route('users.index');
        return 'error';

        //Esto es de la forma antigua
        /*
        $this->validate($request, [
            'nickname' => ['required'],
            'apellidos' => ['required'],
            'nombre' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        //dd($request);
        User::create([
            'nickname' => $request['nickname'],
            'nombre' => $request['nombre'],
            'apellidos' => $request['apellidos'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
        $user = User::where('nickname',$request->nickname)->get()->first();

        return view('auth.users.perfil', compact('user'));
*/
    }


    //Redirige bien y coge bien la variable
    public function edit(User $user)
    {
        $user= $this->userRepository->findById($user->id);
        return view('auth.users.form',compact('user'));
    }

    //Actualiza bien el user, pero hay que hacer cambios con la contraseña en la vista
    public function update(Request $request, User $user)
    {
        //dd($user);
        if($this->userRepository->update($request,$user)) return redirect()->route('users.index');
        return 'Error';
    }


    public function destroy(User $user)
    {
        if($this->userRepository->destroy($user->id)) return redirect()->route('users.index');

        return 'Error';
    }
}
