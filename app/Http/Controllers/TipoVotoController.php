<?php

namespace App\Http\Controllers;

use App\TipoVoto;
use Illuminate\Http\Request;

class TipoVotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoVoto  $tipoVoto
     * @return \Illuminate\Http\Response
     */
    public function show(TipoVoto $tipoVoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoVoto  $tipoVoto
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoVoto $tipoVoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoVoto  $tipoVoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoVoto $tipoVoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoVoto  $tipoVoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoVoto $tipoVoto)
    {
        //
    }
}
