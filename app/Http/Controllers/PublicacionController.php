<?php

namespace App\Http\Controllers;

use App\Voto;
use App\Publicacion;
use App\Repositories\Publicacion\PublicacionInterface;
use Illuminate\Http\Request;

class PublicacionController extends Controller
{

    //Necesario
    protected $publicacionRepository;
    public function __construct(PublicacionInterface $publicacionRepository)
    {
        $this->publicacionRepository = $publicacionRepository;
        $this->middleware('auth');
    }

    //100% funcional
    public function index()
    {
        $publicaciones = $this->publicacionRepository->getAll();
        //dd($publicaciones);


        return view('auth.publicaciones.index',compact('publicaciones'));
    }


    public function show($id)
    {

        $publicacion = $this->publicacionRepository->findById($id);
        //$votos=$this->publicacionRepository->getVotos($id);
        //dd($publicacion);

        //$votos = Voto::where('publicacion_id',$id)->get();
        //dd($votos);

        return view('auth.publicaciones.show', compact('publicacion'));
    }

    public function create()
    {
        return view('auth.publicaciones.form');

    }

    public function store(Request $request)
    {
        if($this->publicacionRepository->store($request)) return redirect()->route('publicaciones.index');
        return 'error';
    }

    public function edit($id)
    {
        $publicacion= $this->publicacionRepository->findById($id);
        //dd($publicacion);
        return view('auth.publicaciones.form',compact('publicacion'));
    }

        public function update(Request $request, $id)
    {
        $publicacion= $this->publicacionRepository->findById($id);
        if($this->publicacionRepository->update($request,$publicacion)) return redirect()->route('publicaciones.index');
        return 'Error';
    }

    public function destroy($id)
    {

        if($this->publicacionRepository->destroy($id)) return redirect()->route('publicaciones.index');

        return 'Error';
    }
}
