<?php

namespace App\Http\Controllers;

use App\Repositories\Sector\SectorInterface;
use App\Sector;
use Illuminate\Http\Request;

class SectoresController extends Controller
{
    protected $sectorRepository;
    public function __construct(SectorInterface $sectorRepository)
    {
        $this->sectorRepository = $sectorRepository;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectores = $this->sectorRepository->getAll();
        return view('auth.sectores.index', compact('sectores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.sectores.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->sectorRepository->create($request)) return redirect()->route('sector.index');
        return 'error';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function show(Sector $sector)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function edit(Sector $sector)
    {
        return view('auth.sectores.form', compact('sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sector  $sector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sector $sector)
    {
        if($this->sectorRepository->update($request, $sector)) return redirect()->route('sector.index');
        return 'error';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->sectorRepository->destroy($id)) return redirect()->route('sector.index');
        return 'error';
    }
}
