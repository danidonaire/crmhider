<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Repositories\Empresa\EmpresaInterface;
use App\Repositories\Sector\SectorInterface;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    protected $empresaRepository;
    protected $sectorRepository;

    public function __construct(EmpresaInterface $empresaRepository, SectorInterface $sectorRepository)
    {
        $this->empresaRepository = $empresaRepository;
        $this->sectorRepository = $sectorRepository;
        $this->middleware('auth');
    }


    public function index()
    {
        $empresas = $this->empresaRepository->getAll();
        return view('auth.empresas.index',compact('empresas'));
    }

    public function create()
    {
        $sectores = $this->sectorRepository->getDisplayNameAndId();
        return view('auth.empresas.form', compact('sectores'));
    }
    public function store(Request $request)
    {
        if($this->empresaRepository->store($request)) return redirect()->route('empresa.index');
        return 'error';
    }

    public function show(Empresa $empresa)
    {
        dd($this->empresaRepository->findById($empresa->id));
        return 'Show Empresa Controller';
    }

    public function edit(Empresa $empresa)
    {
        $sectores = $this->sectorRepository->getDisplayNameAndId();
        return view('auth.empresas.form',compact('empresa','sectores'));
    }

    public function update(Request $request, Empresa $empresa)
    {
        if($this->empresaRepository->update($request,$empresa)) return redirect()->route('empresa.index');
        return 'Error';
    }

    public function destroy($id)
    {
        if($this->empresaRepository->destroy($id)) return redirect()->route('empresa.index');
        return 'error';
    }
}
