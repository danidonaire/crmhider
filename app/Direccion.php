<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Direccion extends Model
{
    protected $table = 'direcciones';
    protected $fillable = [
        'calle', 'numero', 'municipio','poblacion','cp','pais',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class,'direccion_id','id');
    }

    public function getFullStreetAttribute(){

        return
            $this->attributes['calle'].', '.$this->attributes['numero'].' '.
            $this->attributes['municipio'].', '.$this->attributes['cp'].' '.$this->attributes['poblacion'].', '.
            $this->attributes['pais'];
    }
}
