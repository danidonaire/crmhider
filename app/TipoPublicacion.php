<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPublicacion extends Model
{
    protected  $table = 'tipo_publicaciones';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre'
    ];

    public function publicacion(){
        return $this->hasMany(Publicacion::class);
    }
}
