<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    protected  $table = 'publicaciones';

    protected $primaryKey = 'id';

    protected $fillable = [
        'titulo','contenido','reputacion','views','filtro','viral','guardados', 'user_id',
    ];


    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function categoriaPublicacion(){
        return $this->belongsToMany(CategoriaPublicacion::class);
    }
    public function tipoPublicacion(){
        return $this->belongsTo(TipoPublicacion::class);
    }
    /*public function votos(){
        return $this->hasMany(Voto::class);
    }*/

}

