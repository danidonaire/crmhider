<?php

namespace App\Providers;

use App\Repositories\Empresa\EmpresaInterface;
use App\Repositories\Empresa\EmpresaRepository;
use App\Repositories\Permisos\PermisosInterface;
use App\Repositories\Permisos\PermisosRepository;
use App\Repositories\Roles\RoleInterface;
use App\Repositories\Roles\RoleRepository;
use App\Repositories\Sector\SectorInterface;
use App\Repositories\Sector\SectorRepository;
use App\Repositories\User\UserInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\Publicacion\PublicacionInterface;
use App\Repositories\Publicacion\PublicacionRepository;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::resourceVerbs(['create' => 'crear', 'edit' => 'editar', 'destroy' => 'eliminar', 'show' => 'ver', 'update' => 'actualizar', 'store' => 'insertar',]);
        Schema::defaultStringLength(191);
        $this->app->bind(EmpresaInterface::class, EmpresaRepository::class);
        $this->app->bind(SectorInterface::class, SectorRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(PublicacionInterface::class, PublicacionRepository::class);
        $this->app->bind(RoleInterface::class, RoleRepository::class);
        $this->app->bind(PermisosInterface::class, PermisosRepository::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
