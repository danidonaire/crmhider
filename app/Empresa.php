<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'nombre', 'token'
    ];

    public function direccion()
    {
        return $this->belongsTo(Direccion::class,'direccion_id','id');
    }

    public function jefes(){
        return $this->belongsToMany(User::class);
    }

    public function sectores(){
        return $this->belongsToMany(Sector::class);
    }
}
