<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voto extends Model
{
    protected  $table = 'voto';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre','user_id','publicacion_id','tipo_id',
    ];




    public function tipoVoto(){
        return $this->belongsTo(Voto::class);
    }
    public function publicacion(){
        return $this->belongsTo(Publicacion::class, 'publicacion_id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
