<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.dashboard');
});
Route::get('/dashboard', function () {
    return view('auth.dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Users
Route::resource('users' , 'UserController');

//Publicaciones
Route::resource('publicaciones' , 'PublicacionController');
Route::resource('cliente' , 'ClienteController');

Route::resource('empresa', 'EmpresaController');//->except(['destroy']);
Route::resource('sector', 'SectoresController');//->except(['destroy']);
Route::resource('roles', 'RoleController');//->except(['destroy']);
Route::resource('permisos', 'PermissionController');//->except(['destroy']);
